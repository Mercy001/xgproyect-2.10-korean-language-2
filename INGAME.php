<?php

//SERVER GENERALS
$lang['Metal']								= '금속';
$lang['Crystal']							= '수정';
$lang['Deuterium']							= '중수소';
$lang['Darkmatter']							= '메캐쉬';
$lang['Energy']								= '에너지';
$lang['Messages']							= '쪽지';
$lang['write_message']						= '쪽지 보내기';

$lang['type_mission'][1]  					= '공격';
$lang['type_mission'][2]  					= '집중 공격';
$lang['type_mission'][3]  					= '화물 운송';
$lang['type_mission'][4]  					= '함선 배치';
$lang['type_mission'][5]  					= '위치 사수';
$lang['type_mission'][6]  					= '정탐';
$lang['type_mission'][7]  					= '식민지화';
$lang['type_mission'][8]  					= '잔해 수거';
$lang['type_mission'][9]  					= '파괴';
$lang['type_mission'][15] 					= '탐사';

$lang['user_level'] = array (
	'0' => 'Player',
	'1' => 'Moderator',
	'2' => 'Operator',
	'3' => 'Administrator',
);

// GAME.PHP
$lang['see_you_soon']						= '또 봐요!';
$lang['page_doesnt_exist']					= '요청하신 페이지가 존재하지 않습니다.';

//SHORT NAMES FOR COMBAT REPORTS
$lang['tech_rc'] = array (
202 => "S. cargo",
203 => "L. cargo",
204 => "F. light",
205 => "F. heavy",
206 => "Cruiser",
207 => "Battleship",
208 => "Col. Ship",
209 => "Recycler",
210 => "Probes",
211 => "Bomber",
212 => "S. Satellite",
213 => "Destroyer",
214 => "Deathstar",
215 => "Battlecru.",

401 => "Rocket L.",
402 => "Light L.",
403 => "Heavy L.",
404 => "Gauss C.",
405 => "Ion C.",
406 => "Plasma T.",
407 => "S. Dome",
408 => "L. Dome",
);

//----------------------------------------------------------------------------//
//TOPNAV
$lang['tn_vacation_mode']					= 'Vacation mode active until ';
$lang['tn_vacation_mode_active'] 			= "Vacation mode active"; 
$lang['tn_delete_mode']						= 'Your account is in erase mode. The same will be deleted the ';

//----------------------------------------------------------------------------//
//LEFT MENU
$lang['lm_overview']						= '개요';
$lang['lm_galaxy']							= '은하';
$lang['lm_empire']							= '제국';
$lang['lm_fleet']							= '함대';
$lang['lm_buildings']						= '건물';
$lang['lm_research']						= '연구';
$lang['lm_shipshard']						= '조선소';
$lang['lm_defenses']						= '국방';
$lang['lm_resources']						= '자원';
$lang['lm_officiers']						= '장교';
$lang['lm_trader']							= '상인';
$lang['lm_technology']						= '기술';
$lang['lm_messages']						= '쪽지함';
$lang['lm_alliance']						= '동맹';
$lang['lm_buddylist']						= '친구목록';
$lang['lm_notes']							= '메모';
$lang['lm_statistics']						= '통계';
$lang['lm_search']							= '검색';
$lang['lm_options']							= '옵션';
$lang['lm_banned']							= '차단된 유저';
$lang['lm_contact']							= '연락';
$lang['lm_forums']							= '게시판';
$lang['lm_logout']							= '로그아웃';
$lang['lm_administration']					= '관리자';
$lang['lm_game_speed']						= '게임 속도';
$lang['lm_fleet_speed']						= '함대 속도';
$lang['lm_resources_speed']					= '자원 속도';
$lang['lm_queues']							= '큐';

//----------------------------------------------------------------------------//
//OVERVIEW
$lang['ov_newname_error']					= '영문, 숫자만 입력 가능합니다.';
$lang['ov_planet_abandoned']				= '행성 폐기 완료';
$lang['ov_principal_planet_cant_abanone']	= '주 행성은 삭제가 불가능합니다!';
$lang['ov_wrong_pass']						= '잘못된 비밀번호입니다.';
$lang['ov_have_new_message']				= '1개의 새로운 메세지가 있습니다';
$lang['ov_have_new_messages']				= '%m개의 새로운 메세지가 있습니다';
$lang['ov_free']							= 'Free';
$lang['ov_news']							= '뉴스';
$lang['ov_place']							= '순위';
$lang['ov_of']								= 'of';
$lang['ov_planet']							= '행성';
$lang['ov_server_time']						= '서버 시간 ';
$lang['ov_events']							= '이벤트';
$lang['ov_diameter']						= '지름';
$lang['ov_distance_unit']					= 'km';
$lang['ov_temperature']						= '온도';
$lang['ov_aprox']							= 'approx.';
$lang['ov_temp_unit']						= '&#186;C';
$lang['ov_to']								= '으로';
$lang['ov_position']						= '위치';
$lang['ov_points']							= '포인트';
$lang['ov_security_request']				= '보안 쿼리';
$lang['ov_security_confirm']				= '행성 삭제를 허가해 주십시오.';
$lang['ov_with_pass']						= '비밀번호를 입력하여';
$lang['ov_password']						= '비밀번호';
$lang['ov_delete_planet']					= '행성 삭제!';
$lang['ov_your_planet']						= '유저의 행성';
$lang['ov_coords']							= '위치';
$lang['ov_abandon_planet']					= '식민지 포기';
$lang['ov_planet_name']						= '이름';
$lang['ov_actions']							= '기능';
$lang['ov_planet_rename']					= '이름 재설정';
$lang['ov_planet_rename_action']			= '이름 재설정';
$lang['ov_abandon_planet_not_possible']		= '함대가 이동중에는 행성을 버릴 수 없습니다';
$lang['ov_fields']							= '공간';

//----------------------------------------------------------------------------//
//GALAXY
$lang['gl_no_deuterium_to_view_galaxy']		= 'You don\'t have enough deuterium!';
$lang['gl_legend']							= '범례';
$lang['gl_strong_player']					= '강자';
$lang['gl_week_player']						= '약자 ( 뉴비 )';
$lang['gl_vacation']						= '휴가 상태';
$lang['gl_banned']							= '활동 금지';
$lang['gl_inactive_seven']					= '7일간 미활동';
$lang['gl_inactive_twentyeight']			= '28일간 미활동';
$lang['gl_s']								= 's';
$lang['gl_w']								= 'n';
$lang['gl_v']								= 'v';
$lang['gl_b']								= 'b';
$lang['gl_i']								= 'i';
$lang['gl_I']								= 'I';
$lang['gl_populed_planets']					= '행성이 식민지화 됨.';
$lang['gl_out_space']						= '외계 탐사';
$lang['gl_avaible_missiles']				= '미사일 사용가능';
$lang['gl_fleets']							= '함대';
$lang['gl_avaible_recyclers']				= '잔해수거기 사용가능';
$lang['gl_avaible_spyprobes']				= '정탐기 사용가능';
$lang['gl_missil_launch']					= '미사일 발사';
$lang['gl_missil_to_launch']				= '미사일 발사수 (<b>%d</b> 발):';
$lang['gl_all_defenses']					= '전부';
$lang['gl_objective']						= '주 목표';
$lang['gl_missil_launch_action']			= '승인';
$lang['gl_galaxy']							= '은하계';
$lang['gl_solar_system']					= '시스템';
$lang['gl_show']							= '이동';
$lang['gl_pos']								= '번호';
$lang['gl_planet']							= '행성';
$lang['gl_name_activity']					= '행성명(활동)';
$lang['gl_moon']							= '위성';
$lang['gl_debris']							= '잔해';
$lang['gl_player_estate']					= '사용자명(상태)';
$lang['gl_alliance']						= '동맹';
$lang['gl_actions']							= '행동';
$lang['gl_spy']								= '정탐';
$lang['gl_buddy_request']					= '친구 요청';
$lang['gl_missile_attack']					= '미사일 공격';
$lang['gl_with']							= ' consisting of ';
$lang['gl_member']							= ' 멤버';
$lang['gl_member_add']						= '(s)';
$lang['gl_alliance_page']					= '동맹';
$lang['gl_see_on_stats']					= '통계';
$lang['gl_alliance_web_page']				= '동맹의 홈페이지';
$lang['gl_debris_field']					= '잔해 공간';
$lang['gl_collect']							= '잔해 수거';
$lang['gl_resources']						= '자원';
$lang['gl_features']						= '특징';
$lang['gl_diameter']						= '지름';
$lang['gl_temperature']						= '온도';
$lang['gl_phalanx']							= 'Phalanx';
$lang['gl_planet_destroyed']				= '행성 파괴';
$lang['gl_player']							= '사용자';
$lang['gl_in_the_rank']						= ' 순위 ';
$lang['gl_stat']							= '통계';

//----------------------------------------------------------------------------//
//PHALANX
$lang['px_no_deuterium']					= '중수소가 부족합니다!';
$lang['px_scan_position']					= '지역을 탐색합니다';
$lang['px_fleet_movement']					= '함대가 부족합니다';

//----------------------------------------------------------------------------//
//IMPERIUM
$lang['iv_imperium_title']					= '제국의 정보';
$lang['iv_planet']							= '행성';
$lang['iv_name']							= '이름';
$lang['iv_coords']							= '좌표';
$lang['iv_fields']							= '필드';
$lang['iv_resources']						= '자원';
$lang['iv_buildings']						= '건설물';
$lang['iv_technology']						= '기술';
$lang['iv_ships']							= '함선';
$lang['iv_defenses']						= '방어';

//----------------------------------------------------------------------------//
//FLEET - FLEET1 - FLEET2 - FLEET3 - FLEETACS - FLEETSHORTCUTS
$lang['fl_returning']						= '귀환 중';
$lang['fl_onway']							= '가는 중 입니다';
$lang['fl_r']								= '(R)';
$lang['fl_a']								= '(A)';
$lang['fl_send_back']						= '후퇴';
$lang['fl_acs']								= 'ACS';
$lang['fl_no_more_slots']					= '사용 가능한 함대 슬롯이 없습니다.';
$lang['fl_speed_title']						= '속도: ';
$lang['fl_continue']						= '다음';
$lang['fl_no_ships']						= '사용 가능한 함선이 없습니다.';
$lang['fl_remove_all_ships']				= '모든 배치 취소';
$lang['fl_select_all_ships']				= '전 함선 배치';
$lang['fl_fleets']							= '함대';
$lang['fl_expeditions']						= '탐사대';
$lang['fl_number']							= 'ID';
$lang['fl_mission']							= '임무';
$lang['fl_ammount']							= '함선 (합계)';
$lang['fl_beginning']						= '출발 좌표';
$lang['fl_departure']						= '출발 시간';
$lang['fl_destiny']							= '목적 좌표';
$lang['fl_objective']						= '도착 시간';
$lang['fl_arrival']							= '복귀 시간';
$lang['fl_order']							= '명령';
$lang['fl_new_mission_title']				= '함대에 편성시킬 함선을 선택하십시오.';
$lang['fl_ship_type']						= '함선 종류';
$lang['fl_ship_available']					= '사용 가능';
$lang['fl_planet']							= '행성';
$lang['fl_debris']							= '잔해';
$lang['fl_moon']							= '위성';
$lang['fl_planet_shortcut']					= '(P)';
$lang['fl_debris_shortcut']					= '(D)';
$lang['fl_moon_shortcut']					= '(M)';
$lang['fl_no_shortcuts']					= 'No shortcuts';
$lang['fl_anonymous']						= '익명';
$lang['fl_shortcut_add_title']				= '이름 [Galaxy / System / Planet]';
$lang['fl_shortcut_name']					= '이름';
$lang['fl_shortcut_galaxy']					= '은하';
$lang['fl_shortcut_solar_system']			= '태양계';
$lang['fl_clean']							= '삭제';
$lang['fl_register_shorcut']				= '등록';
$lang['fl_shortcuts']						= '지름길';
$lang['fl_reset_shortcut']					= '초기화';
$lang['fl_dlte_shortcut']					= '삭제';
$lang['fl_back']							= '뒤로가기';
$lang['fl_shortcut_add']					= '추가';
$lang['fl_shortcut_edition']				= '편집: ';
$lang['fl_no_colony']						= '식민지가 없습니다';
$lang['fl_send_fleet']						= '함대 보내기';
$lang['fl_fleet_speed']						= '속도';
$lang['fl_distance']						= '거리';
$lang['fl_flying_time']						= '비행 시간';
$lang['fl_fuel_consumption']				= '연료 소비량';
$lang['fl_max_speed']						= '최대 속도';
$lang['fl_cargo_capacity']					= '화물 운송가능량';
$lang['fl_shortcut']						= '지름길';
$lang['fl_shortcut_add_edit']				= '(추가 / 편집)';
$lang['fl_my_planets']						= '행성';
$lang['fl_acs_title']						= '집중 공격';
$lang['fl_hold_time']						= '체류 시간';
$lang['fl_resources']						= '자원';
$lang['fl_max']								= '최대';
$lang['fl_resources_left']					= '남은 자원';
$lang['fl_all_resources']					= '모든 자원';
$lang['fl_expedition_alert_message']		= '경고! 탐사 도중 당신의 탐사대가 파괴될 수 있습니다!';
$lang['fl_vacation_mode_active']			= '휴가 설정 완료';
$lang['fl_expedition_tech_required']		= '탐사 기술이 필요합니다!';
$lang['fl_expedition_fleets_limit']			= '탐사대의 최대 한도에 도달했습니다!!';
$lang['fl_week_player']						= '플레이어가 너무 약합니다!';
$lang['fl_strong_player']					= '플레이어가 너무 강합니다!';
$lang['fl_in_vacation_player']				= '플레이어가 휴가 중 입니다!';
$lang['fl_no_slots']						= '슬롯 최대 한도에 도달했습니다';
$lang['fl_empty_transport']					= '0 초과 자원만 수송 가능합니다';
$lang['fl_planet_populed']					= '이 행성은 점령되었습니다!';
$lang['fl_stay_not_on_enemy']				= '적의 행성에 함대를 정박할 수 없습니다!';
$lang['fl_not_ally_deposit']				= '동맹 창고가 없습니다';
$lang['fl_deploy_only_your_planets']		= '당신의 행성에만 함대를 정박할 수 있습니다!';
$lang['fl_no_enought_deuterium']			= '중수소가 부족합니다. 중수소 소비량: ';
$lang['fl_no_enought_cargo_capacity']		= '창고에 공간이 부족합니다. 여유 용량:';
$lang['fl_admins_cannot_be_attacked']		= '관리자를 공격 할 수 없습니다.';
$lang['fl_fleet_sended']					= '출발한 함대';
$lang['fl_from']							= '출발 좌표';
$lang['fl_arrival_time']					= '출발 시간';
$lang['fl_return_time']						= '도착 시간';
$lang['fl_fleet']							= '함대';
$lang['fl_player']							= '사용자 ';
$lang['fl_add_to_attack']					= ' 가 공격 대상에 추가되었습니다.';
$lang['fl_dont_exist']						= ' 가 공격 대상에 추가되지 않았습니다.';
$lang['fl_acs_invitation_message']			= ' ACS에 초대되었습니다.';
$lang['fl_acs_invitation_title']			= 'ACS 초대';
$lang['fl_sac_of_fleet']					= 'ACS 함대';
$lang['fl_modify_sac_name']					= 'ACS의 이름을 바꿉니다';
$lang['fl_members_invited']					= '멤버가 초대되었습니다';
$lang['fl_invite_members']					= '다른 멤버 초대';
$lang['fl_hours']							= '시간';

//----------------------------------------------------------------------------//
//BUILDINGS - RESEARCH - SHIPYARD - DEFENSES
$lang['bd_dismantle']						= 'Dismantle';
$lang['bd_interrupt']						= 'Interrupt';
$lang['bd_cancel']							= '취소';
$lang['bd_working']							= '작업 중..';
$lang['bd_build']							= '건설';
$lang['bd_build_next_level']				= '건설 레벨 ';
$lang['bd_add_to_list']						= '건설 명단에 추가';
$lang['bd_no_more_fields']					= '행성에 공간이 부족합니다.';
$lang['bd_remaining']						= 'Remaining';
$lang['bd_lab_required']					= '이 기능을 위해서는 행성에 연구소가 필요합니다!';
$lang['bd_building_lab']					= '연구소를 건설 중일 때는 연구가 불가능합니다.';
$lang['bd_lvl']								= '레벨';
$lang['bd_spy']								= ' 정탐';
$lang['bd_commander']						= ' 지시';
$lang['bd_research']						= '연구';
$lang['bd_shipyard_required']				= '이 기능을 위해서는 행성에 조선소가 필요합니다!';
$lang['bd_building_shipyard']				= '조선소를 건설 중일 때는 제작이 불가능합니다.';
$lang['bd_available']						= '수량: ';
$lang['bd_build_ships']						= '생산';
$lang['bd_protection_shield_only_one']		= '방어막 생성기는 오직 하나만 생산이 가능합니다!';
$lang['bd_build_defenses']					= '생산';
$lang['bd_actual_production']				= '남은 시간:';
$lang['bd_completed']						= '완료';
$lang['bd_operating']						= '(생산 중..)';
$lang['bd_continue']						= '계속';
$lang['bd_ready']							= '완료';
$lang['bd_finished']						= '완료';
$lang['bd_work_todo']						= 'Construction queue';

//----------------------------------------------------------------------------//
//RESOURCES
$lang['rs_amount']							= 'quantity';
$lang['rs_lvl']								= '레벨';
$lang['rs_production_on_planet']			= '자원 생산량 행성 "%s"';
$lang['rs_basic_income']					= '기본 생산량';
$lang['rs_storage_capacity']				= '최대 저장량';
$lang['rs_calculate']						= '적용';
$lang['rs_sum']								= '총 생산량:';
$lang['rs_daily']							= '일일 생산량:';
$lang['rs_weekly']							= '주일 생산량:';

//----------------------------------------------------------------------------//
//OFFICIERS
$lang['of_title']							= '장교';
$lang['of_recruit']							= 'Recruit';
$lang['of_active']							= '<strong><font color="lime">Active</font></strong>';
$lang['of_inactive']						= '<strong><font color="red">Inactive</font></strong>';

//----------------------------------------------------------------------------//
//TRADER
$lang['tr_only_positive_numbers']			= 'You may only use positive numbers!';
$lang['tr_not_enought_metal']				= 'You do not have enough metal.';
$lang['tr_not_enought_crystal']				= 'You do not have enough crystal.';
$lang['tr_not_enought_deuterium']			= 'You do not have enough deuterium.';
$lang['tr_exchange_done']					= 'Exchange successful';
$lang['tr_call_trader']						= 'Call a dealer';
$lang['tr_call_trader_who_buys']			= 'Call a dealer who buy ';
$lang['tr_call_trader_submit']				= 'Call trader';
$lang['tr_exchange_quota']					= 'The exchange rate is 2/1/0.5';
$lang['tr_sell_metal']						= 'Sales of metal';
$lang['tr_sell_crystal']					= 'Sales of crystal';
$lang['tr_sell_deuterium']					= 'Sales of deuterium';
$lang['tr_resource']						= 'Resource';
$lang['tr_amount']							= 'Quantity';
$lang['tr_quota_exchange']					= 'Exchange rate';
$lang['tr_exchange']						= 'Swap';
$lang['tr_darkmatter_needed']				= 'You need at least <font color="#FF8900">%s of dark matter</font> too visit the dealer.';
$lang['tr_full_storage']					= 'The %s is full';
$lang['tr_must_put_something']				= 'You cannot exchange 0 resources';

//----------------------------------------------------------------------------//
//TECHTREE
$lang['tt_requirements']					= '필요 조건';
$lang['tt_lvl']								= '레벨 ';

$lang['tech'] = array(
0 => "건물",
1 => "금속 광산",
2 => "수정 광산",
3 => "중수소 합성소",
4 => "태양광 발전소",
12 => "핵 융합로",
14 => "로봇 공장",
15 => "나노머신 공장",
21 => "조선소",
22 => "금속 저장고",
23 => "수정 저장고",
24 => "중수소 저장고",
31 => "연구소",
33 => "테라포머",
34 => "동맹 보급고",
40 => "위성 건축물",
41 => "위성 기지",
42 => "다중 센서",
43 => "점프 게이트",
44 => "미사일 격납고",

100 => "연구",
106 => "정탐 기술",
108 => "컴퓨터 기술",
109 => "군사 기술",
110 => "방어막 기술",
111 => "합금 기술",
113 => "에너지 기술",
114 => "초 공간 기술",
115 => "연소 드라이브",
117 => "임펄스 드라이브",
118 => "초 공간 드라이브",
120 => "광선 기술",
121 => "이온 기술",
122 => "플라즈마 기술",
123 => "은하 간 통신망",
124 => "탐사 기술",
199 => "중력자 기술",

200 => "조선소",
202 => "소형 화물선",
203 => "대형 화물선",
204 => "경량 전투기",
205 => "중량 전투기",
206 => "순양함",
207 => "전투선",
208 => "식민지선",
209 => "잔해 수거선",
210 => "정탐기",
211 => "폭격기",
212 => "태양광 위성",
213 => "파괴선",
214 => "죽음의 별",
215 => "전투순양함",

400 => "행성 방어",
401 => "지대공 로켓 발사기",
402 => "경량 광선포",
403 => "중량 광선포",
404 => "가우스 포",
405 => "이온 포",
406 => "플라즈마 포탑",
407 => "소형 방어막 생성기",
408 => "대형 방어막 생성기",
502 => "요격 미사일 [ABM]",
503 => "행성간 미사일 [IPM]",

600 => "장교",
601 => "Geologist",
602 => "Admiral",
603 => "Engineer",
604 => "Technocrat",
);

$lang['res']['descriptions'] = array(
1 => "금속 광산과 거기서 나온 금속은 신흥 제국이든 부흥한 제국이든 가장 중요한 자원입니다.",
2 => "수정은 전자 회로를 구성하는 화합물을 생성하기 위한 주된 자원입니다.",
3 => "중수소는 우주선의 연료로서 사용되며, 심해에서 얻어집니다. 중수소는 희귀하기 때문에 상대적으로 고가입니다.",
4 => "태양광 발전소는 태양 복사 에너지를 흡수합니다. 모든 광산 운영에는 에너지가 필요합니다.",
12 => " 매우 높은 압력과 높은 온도에서 두 중수소 원자를 핵융합하여 헬륨 원자와 에너지를 생성합니다 .",
14 => "공장건설 로봇은 건물의 건설에 도움을 제공합니다. 로봇 공장의 레벨이 올라갈수록 건물 업그레이드의 속도가 빨라집니다.",
15 => "나노 로봇은 로봇 공학의 종착지입니다. 나노 로봇 공장의 레벨이 올라갈수록 건설 속도가 올라갑니다.",
21 => "격납고는 건설된 함선과 행성 방어 기지가 있는곳입니다.",
22 => "금속 저장고가 확장됩니다.",
23 => "수정 저장고가 확장됩니다.",
24 => "중수소 저장고가 확장됩니다",
31 => "연구소는 새로운 기술에 대한 연구를 위하여 필요합니다.",
33 => "테라포머는 인프라를 구축할 행성의 접근 가능한 영역을 확장하는데 필요합니다.",
34 => "방어를 도와주는 동맹군의 함대에 연로를 공급할 수 있습니다.",
41 => "달 기지는 대기가 없는 달에 생활 공간을 생성하는데 사용됩니다.",
42 => "다중 센서를 사용함으로서 다른 제국의 함대를 발견하고 관찰할 수 있습니다. 더 많은 센서는 더 넓은 범위의 수색이 가능합니다.",
43 => "양자 도약은 짧은 시간내에 공간을 건너뛰어 이동할수 있습니다.",
44 => "사일로는 IPM과 ABM을 저장하고 발사하는 장소입니다.",

106 => "타 행성을 정탐하여 정보를 얻을 수 있는 기술입니다.",
108 => "각 레벨마다 함대 편성 가능 숫자가 1씩 증가합니다.",
109 => "모든 함선의 무기 피해도가 각 레벨마다 10%씩 증가합니다.",
110 => "모든 방어막의 내구도가 각 레벨마다 10%씩 증가합니다. ",
111 => "모든 함선의 장갑 내구도가 각 레벨마다 10%씩 증가합니다.",
113 => "다른 형태의 에너지 기술의 이해는 많은 전문적이고 새로운 연구에 적용될수 있습니다. 에너지 기술은 현대 연구기관에 중요합니다.",
114 => "4차원과 5차원에서의 추진 기술을 융합하여서 탄생한 새로운 종류의 모터는 더 효과적이고 구형 모터보다 적은 연료를 사용합니다.",
115 => "연소 엔진을 장착한 함선들의 속도가 각 레벨마다 10%씩 증가합니다.",
117 => "임펄스 엔진을 장착한 함선들의 속도가 각 레벨마다 20%씩 증가합니다.",
118 => "초 공간 엔진을 장착한 함선들의 속도가 각 레벨마다 30%씩 증가합니다.",
120 => "The laser technology is an important knowledge, leads to monochromatic light tightly focused on a target. The damage can be mild or moderate depending on the power of lightning ...",
121 => "The Ionic technology focuses a beam of accelerated ions in a target, which can cause great damage because of the nature of energized electrons.",
122 => "plasma weapons are even more dangerous than any known weapon system due to the aggressive nature of plasma.",
123 => "Scientists of your planets can communicate with each other through this network.",
124 => "The ships are equipped with scientific equipment to collect data on long expeditions.",
199 => "Through the firing of graviton particles concentrated gravitational field is generated artificially with enough power and attractiveness to destroy not only ships but entire moons.",

202 => "The small ships are cargo ships used to transport very agile resources from one planet to another.",
203 => "The big ship loading is an advanced version of the small cargo ships, allowing for greater storage capacity and higher speeds through improved propulsion system.",
204 => "The hunter is a lightweight maneuverable craft that you can find in almost any planet. The cost is not particularly high, but also the shield and carrying capacity are very low.",
205 => "The Hunter heavy is the logical evolution of lightweight shields provide reinforced and increased attack power.",
206 => "The battle cruisers have a shield nearly three times stronger than the heavier and more hunters double attack power. Its travel speed is also among the more rational ; ask for ever.",
207 => "The battleships are the backbone of any military fleet. Heavy armor, powerful weapons systems and high travel speed and a high load capacity makes this ship a tough opponent to fight against.",
208 => "This ship provides a facility to go where no man has gone before and colonize new worlds.",
209 => "The recyclers are used to collect debris floating in space to recycle them into useful resources.",
210 => "espionage probes are small unmanned droids with an exceptionally fast propulsion system used to spy on enemy planets.",
211 => "The Bomber is a special purpose ship, developed to cut through heavier planetary defenses.",
212 => "Are simple solar satellites orbiting satellites equipped with photovoltaic panels and transmitters to bring energy to the planet. Hereby is transmitted to the ground using a special laser.",
213 => "The destroyer is the heaviest spacecraft ever seen and has an unprecedented attack potential.",
214 => "There is nothing as big and dangerous as an approaching Death Star.",
215 => "Battleship is a highly specialized craft to intercept hostile fleets.",

401 => "The missile defense system is a simple but cheap.",
402 => "Through a focused laser beam, it can cause more damage than normal ballistic weapons.",
403 => "large lasers have an output better energi'ay greater structural integrity than the small lasers.",
404 => "Using a vast electromagnetic acceleration, gauss cannons accelerate heavy projectiles.",
405 => "The ion cannons fired high-energy ion beams its target, destabilizing and destroying shields the electronics.",
406 => "The Guns of plasma free energy of a small solar flare in a ball of plasma. Destructive energy is even higher than that of the Destroyer.",
407 => "Small Shield Dome covers the planet with a thin protective shield that can absorb huge amounts of energy.",
408 => "The great dome of protection comes from an enhanced defense technology that absorbs even more energy before collapsing.",
502 => "Destroy interceptor missiles interplanetary missiles.",
503 => "The interplanetary missile defense systems destroy the enemy.",

601 => 'Geologist is an expert in mining and astro crystallography. Attend their equipment in metallurgy and chemistry and is also responsible for interplanetary communications to optimize the use and refinement of raw material throughout the empire.<br><br><strong><font color="lime">+10% mine production</font></strong>',
602 => 'The Admiral is an experienced veteran and a skillful strategist. The hardest battle is able to get an idea of the situation and contact their admirals subordinates. A wise emperor could rely on their help during the fighting.<br><br><strong><font color="lime">+2 max. fleet slots</font></strong>',
603 => 'The Engineer is a specialist in energy management. In peacetime, it increases the energy of all the colonies. In case of attack, ensuring the supply of energy to the cannons, and avoid a possible overload, leading to a reduction in defense lost battle.<br><br><strong><font color="lime">Minimizes losses in half defenses<br />+10% energy production</font></strong>',
604 => 'The guild is composed of technocrats authentic genius, and always find that dangerous edge where it all explode into a thousand pieces before they could find an explanation and technology rational. No normal human being ever try to decode the code of a technocrat, with its presence, the researchers modeled the rule.<br><br><strong><font color="lime">+2 Espionage level<br/>-25% Less time for research</font></strong>',
);

//----------------------------------------------------------------------------//
//INFOS
$lang['in_jump_gate_done']					= 'The jump gate was used, the next jump can be made in: ';
$lang['in_jump_gate_error_data']			= 'Error, data for the jump are not correct!';
$lang['in_jump_gate_not_ready_target']		= 'The jump gate is not ready on the finish moon, will be ready in ';
$lang['in_jump_gate_doesnt_have_one']		= 'You have no jump gate in the moon!';
$lang['in_jump_gate_already_used']			= 'The jump gate was used, time to recharge its energy: ';
$lang['in_jump_gate_available']				= 'available';
$lang['in_rf_again']    					= 'Rapidfire against';
$lang['in_rf_from']     					= 'Rapidfire from';
$lang['in_level']       					= 'Level';
$lang['in_prod_p_hour'] 					= 'production/hour';
$lang['in_difference']  					= 'Difference';
$lang['in_used_energy'] 					= 'Energy consumption';
$lang['in_prod_energy'] 					= 'Energy Production';
$lang['in_used_deuter']						= 'Deuterium consumption';
$lang['in_range']       					= '감지 범위';
$lang['in_title_head']  					= 'Information of';
$lang['in_name']        					= 'Name';
$lang['in_struct_pt']   					= 'Structural Integrity';
$lang['in_shield_pt']   					= 'Shield Strength';
$lang['in_attack_pt']   					= 'Attack Strength';
$lang['in_capacity']    					= 'Cargo Capacity';
$lang['in_units']       					= 'units';
$lang['in_base_speed'] 						= 'Base speed';
$lang['in_consumption'] 					= 'Fuel usage (Deuterium)';
$lang['in_jump_gate_start_moon']			= 'Start moom';
$lang['in_jump_gate_finish_moon']			= 'Finish moon';
$lang['in_jump_gate_select_ships']			= 'Use Jump Gate: number of ships';
$lang['in_jump_gate_jump']					= 'Jump';
$lang['in_destroy']     					= 'Destroy:';
$lang['in_needed']      					= 'Requires';
$lang['in_dest_durati'] 					= 'Destruction time';

// -------------------------- MINES ------------------------------------------------------------------------------------------------------//
$lang['info'][1]['name']          			= '금속 광산';
$lang['info'][1]['description']   			= '금속은 당신의 제국을 건설하는데 있어 중요한 자원입니다. 금속은 건설, 함선 건조, 방어 시스템, 그리고 건설에 있어 매우 중요하게 사용되는 자원입니다. 보다 깊게 파들어 갈수록 더 많은 금속을 얻을수 있으며 동시에 많은 에너지를 필요로 하게됩니다. 금속은 가장 흔하기 때문에 자원 거래시에 가장 낮게 평가됩니다.';
$lang['info'][2]['name']          			= '수정 광산';
$lang['info'][2]['description']   			= '수정은 컴퓨터를 위한 전기회로나 보호막에 사용되는 화합물을 만드는데 중요한 자원입니다. 금속 생산 과정과 비교하였을 때 수정 원석을 공엉용으로 바꾸는데 특별한 과정이 필요합니다. 결국  수정 생산은 금속 생산보다 더 많은 에너지를 필요로합니다. 함선과 건물을 그리고 전문적인 연구를 하는데 상당한 수정이 요구됩니다.';
$lang['info'][3]['name']          			= '중수소 합성소';
$lang['info'][3]['description']   			= '중수소는 무거운 수소로 불려집니다. 중수소는 수소의 안정된 동위원소로 바다에 풍부하게 존재하고 있습니다 일반수소와의 비율은 수소 원자 6500개당 중수소 원자 1개에 해당합니다. 따라서 원자 개수로 보면 전체의 0.015%를 차지합니다. 물에서 중수소를 분리할 수 있도록 특별하게 설계된 원심 분리기에 의해 정제가 됩니다. 합성소를 업그레이드 하면 중수소의 생산량이 증가하게됩니다. 중수소는 밀집 센서를 작동하거나 은하를 탐색하는데 사용되고, 함선의 연료로 사용되거나, 연구에 이용되기도 합니다.';

// -------------------------- ENERGY ----------------------------------------------------------------------------------------------------//
$lang['info'][4]['name']          			= "태양광 발전소";
$lang['info'][4]['description']   			= "Gigantic solar arrays are used to generate power for the mines and the deuterium synthesizer. As the solar plant is upgraded, the surface area of the photovoltaic cells covering the planet increases, resulting in a higher energy output across the power grids of your planet.";
$lang['info'][12]['name']         			= "핵 융합로";
$lang['info'][12]['description']  			= "In fusion power plants, hydrogen nuclei are fused into helium nuclei under enormous temperature and pressure, releasing tremendous amounts of energy. For each gram of Deuterium consumed, up to 41,32*10^-13 Joule of energy can be produced; with 1 g you are able to produce 172 MWh energy.<br><br>Larger reactor complexes use more deuterium and can produce more energy per hour. The energy effect could be increased by researching energy technology.<br><br>The energy production of the fusion plant is calculated like that:<br>30 * [Level Fusion Plant] * (1,05 + [Level Energy Technology] * 0,01) ^ [Level Fusion Plant]";

// -------------------------- BUILDINGS ----------------------------------------------------------------------------------------------------//
$lang['info'][14]['name']         			= "로봇 공장";
$lang['info'][14]['description']  			= "The Robotics Factory primary goal is the production of State of the Art construction robots. Each upgrade to the robotics factory results in the production of faster robots, which is used to reduce the time needed to construct buildings.";
$lang['info'][15]['name']         			= "나노머신 공장";
$lang['info'][15]['description']  			= "A nanomachine, also called a nanite, is a mechanical or electromechanical device whose dimensions are measured in nanometers (millionths of a millimeter, or units of 10^-9 meter). The microscopic size of nanomachines translates into higher operational speed. This factory produces nanomachines that are the ultimate evolution in robotics technology. Once constructed, each upgrade significantly decreases production time for buildings, ships, and defensive structures.";
$lang['info'][21]['name']         			= "조선소";
$lang['info'][21]['description']  			= "The planetary shipyard is responsible for the construction of spacecraft and defensive mechanisms. As the shipyard is upgraded, it can produce a wider variety of vehicles at a much greater rate of speed. If a nanite factory is present on the planet, the speed at which ships are constructed is massively increased.";
$lang['info'][22]['name']         			= "금속 광산";
$lang['info'][22]['description']  			= "This storage facility is used to store metal ore. Each level of upgrading increases the amount of metal ore that can be stored. If the storage capacity is exceeded, the metal mines are automatically shut down to prevent a catastrophic collapse in the metal mine shafts.";
$lang['info'][23]['name']         			= "수정 광산";
$lang['info'][23]['description']  			= "Raw crystal is stored in this building. With each level of upgrade, it increases the amount of crystal can be stored. Once the mines output exceeds the storage capacity, the crystal mines automatically shut down to prevent a collapse in the mines.";
$lang['info'][24]['name']         			= "중수소 합성소";
$lang['info'][24]['description']  			= "The Deuterium tank is for storing newly-synthesized deuterium. Once it is processed by the synthesizer, it is piped into this tank for later use. With each upgrade of the tank, the total storage capacity is increased. Once the capacity is reached, the Deuterium Synthesizer is shut down to prevent the tanks rupture.";
$lang['info'][31]['name']         			= "연구소";
$lang['info'][31]['description']  			= "An essential part of any empire, Research Labs are where new technologies are discovered, and older technologies are improved upon. With each level of the Research Lab constructed, the speed in which new technologies are researched is increased, while also unlocking newer technologies to research. In order to conduct research as quickly as possible, research scientists are immediately dispatched to the colony to begin work and development. In this way, knowledge about new technologies can easily be disseminated throughout the empire.";
$lang['info'][33]['name']         			= "테라포머";
$lang['info'][33]['description']  			= "With the ever increasing mining of a colony, a problem arose. How can we continue to operate at a planets capacity and still survive? The land is being mined out and the atmosphere is deteriorating. Mining a colony to capacity can not only destroy the planet, but may kill all life on it. Scientists working feverishly discovered a method of creating enormous land masses using nanomachines. The Terraformer was born.<br><br>Once built, the Terraformer cannot be torn down.";
$lang['info'][34]['name']         			= "동맹 보급소";
$lang['info'][34]['description']  			= "The alliance depot supplies fuel to friendly fleets in orbit helping with defense. For each upgrade level of the alliance depot, 10,000 units of deuterium per hour can be sent to an orbiting fleet.";
$lang['info'][41]['name']         			= "위성 기지";
$lang['info'][41]['description']  			= "Since a moon has no atmosphere and is an extremely hostile environment, a lunar base must first be built before the moon can be developed. The Lunar Base provides oxygen, heating, and gravity to create a living environment for the colonists. With each level constructed, a larger living and development area is provided within the biosphere. With each level of the Lunar Base constructed, three fields are developed for other buildings. <br>Once built, the lunar base can not be torn down.";
$lang['info'][42]['name']         			= "다중 센서";
$lang['info'][42]['description']  			= "Utilizing high-resolution sensors, the Sensor Phalanx first scans the spectrum of light, composition of gases, and radiation emissions from a distant world and transmits the data to a supercomputer for processing. Once the information is obtained, the supercomputer compares changes in the spectrum, gas composition, and radiation emissions, to a base line chart of known changes of the spectrum created by various ship movements. The resulting data then displays activity of any fleet within the range of the phalanx. To prevent the supercomputer from overheating during the process, it is cooled by utilizing 5k of processed Deuterium. To use the Phalanx, click on any planet in the Galaxy View within your sensors range.";
$lang['info'][43]['name']         			= "점프 게이트";
$lang['info'][43]['description']  			= "A Jump Gate is a system of giant transceivers capable of sending even the largest fleets to a receiving Gate anywhere in the universe without loss of time. Utilizing technology similar to that of a Worm Hole to achieve the jump, deuterium is not required. A recharge period of one hour must pass between jumps to allow for regeneration. Transporting resources through the Gate is not possible.";
$lang['info'][44]['name']         			= "미사일 격납고";
$lang['info'][44]['description']  			= "When Earth destroyed itself in a full scale nuclear exchange back in the 21st century, the technology needed to build such weapons still existed in the universe. Scientists all over the universe worried about the threat of a nuclear bombardment from a rogue leader. So it was decided to use the same technology as a deterrent from launching such a horrible attack.<br><br> Missile silos are used to construct, store and launch interplanetary and anti-ballistic missiles. With each level of the silo, five interplanetary missiles or ten anti-ballistic missiles can be stored. Storage of both Interplanetary missiles and Anti-Ballistic missiles in the same silo is allowed.";

// -------------------------- TECHNOLOGY ----------------------------------------------------------------------------------------------------//
$lang['info'][106]['name']        			= "정탐 기술";
$lang['info'][106]['description'] 			= 'Espionage Technology is your intelligence gathering tool. This technology allows you to view your targets resources, fleets, buildings, and research levels using specially designed probes. Launched on your target, these probes transmit back to your planet an encrypted data file that is fed into a computer for processing. After processing, the information on your target is
then displayed for evaluation.<br><br> With Espionage Technology, the level of your technology to that of your target is critical. If your target has a higher level of Espionage Technology than you, you will need to launch more probes to gather all the information on your target. However this runs the great risk of detection by your target, resulting in the probes destruction. However, launching too few probes will result in missing information that is most critical, which could result in the total destruction of your fleet if an attack is launched.<br><br>At certain levels of Espionage Technology research, new attack warning systems are installed:<br><br>At Level <font color="#ff0000">2</font>, the total number of attacking ships will be displayed along with the simple attack warning.<br>At Level <font color="#ff0000">4</font>, the type of attacking ships along with the number of ships are displayed.
<br>At Level <font color="#ff0000">8</font>, the exact number of each type of ship launched is displayed.';
$lang['info'][108]['name']        			= "컴퓨터 기술";
$lang['info'][108]['description'] 			= "Once launched on any mission, fleets are controlled primarily by a series of computers located on the originating planet. These massive computers calculate the exact time of arrival, controls course corrections as needed, calculates trajectories, and regulates flight speeds. <br><br>With each level researched, the flight computer is upgraded to allow an additional slot to be launched. Computer technology should be continuously developed throughout the building of your empire.";
$lang['info'][109]['name']        			= "군사 기술";
$lang['info'][109]['description'] 			= "Weapons Technology is a key research technology and is critical to your survival against enemy Empires. With each level of Weapons Technology researched, the weapons systems on ships and your defense mechanisms become increasingly more efficient. Each level increases the base strength of your weapons by 10% of the base value.";
$lang['info'][110]['name']        			= "방어막 기술";
$lang['info'][110]['description'] 			= "With the invention of the magnetosphere generator, scientists learned that an artificial shield could be produced to protect the crew in space ships not only from the harsh solar radiation environment in deep space, but also provide protection from enemy fire during an attack. Once scientists finally perfected the technology, a magnetosphere generator was installed on all ships and defence systems. <br><br>As the technology is advanced to each level, the magnetosphere generator is upgraded which provides an additional 10% strength to the shields base value.";
$lang['info'][111]['name']        			= "합금 기술";
$lang['info'][111]['description'] 			= "The environment of deep space is harsh. Pilots and crew on various missions not only faced intense solar radiation, they also faced the prospect of being hit by space debris, or destroyed by enemy fire in an attack. With the discovery of an aluminum-lithium titanium carbide alloy, which was found to be both light weight and durable, this afforded the crew a certain degree of protection. With each level of Armour Technology developed, a higher quality alloy is produced, which increases the armours strength by 10%.";
$lang['info'][113]['name']        			= "에너지 기술";
$lang['info'][113]['description'] 			= "As various researches were advancing, it was discovered that the current technology of energy distribution was not sufficient enough to begin certain specialized researches. With each upgrade of your Energy Technology, new researches can be conducted which unlocks development of more sophisticated ships and defenses.";
$lang['info'][114]['name']        			= "초 공간 기술";
$lang['info'][114]['description'] 			= 'In theory, the idea of hyperspace travel relies on the existence of a separate and adjacent dimension. When activated, a hyperspace drive shunts the starship into this other dimension, where it can cover vast distances in an amount of time greatly reduced from the time it would take in "normal" space. Once it reaches the point in hyperspace that corresponds to its destination in real space, it re-emerges.<br> Once a sufficient level of Hyperspace Technology is researched, the Hyperspace Drive is no longer just a theory.';
$lang['info'][115]['name']        			= "연소 드라이브";
$lang['info'][115]['description'] 			= "The Combustion Drive is the oldest of technologies, but is still in use. With the Combustion Drive, exhaust is formed from propellants carried within the ship prior to use. In a closed chamber, the pressures are equal in each direction and no acceleration occurs. If an opening is provided at the bottom of the chamber then the pressure is no longer opposed on that side. The remaining pressure gives a resultant thrust in the side opposite the opening, which propels the ship forward by expelling the exhaust rearwards at extreme high speed.<br> <br>With each level of the Combustion Drive developed, the speed of small and large cargo ships, light fighters, recyclers, and espionage probes are increased by 10%.";
$lang['info'][117]['name']        			= "임펄스 드라이브";
$lang['info'][117]['description'] 			= 'The impulse drive is essentially an augmented fusion rocket, usually consisting of a fusion reactor,an accelerator-generator, a driver coil assembly and a vectored thrust nozzle to direct the plasma exhaust. The fusion reaction generates a highly energized plasma. This plasma, ("electro-plasma") can be employed for propulsion, or can be diverted through the EPS to the power transfer grid, via EPS conduits, so as to supply other systems. The accelerated plasma is passed through the driver coils, thereby generating a subspace field which improves the propulsive effect. <br><br>With each level of the Impulse Drive developed, the speed of bombers, cruisers, heavy fighters, and colony ships are increased by 20% of the base value. Interplanetary missiles also travel farther with each level.';
$lang['info'][118]['name']        			= "초 공간 드라이브";
$lang['info'][118]['description'] 			= "With the advancement of Hyperspace Technology, the Hyperspace Drive was created. Hyperspace is an alternate region of space co-existing with our own universe which may be entered using an energy field or other device. The HyperSpace Drive utilizes this alternate region by distorting the space-time continuum, which results in speeds that exceed the speed of light (otherwise known as FTL travel). During FTL travel, time and space is warped to the point that results in a trip that would normally take 1000 light years to be completed, to be accomplished in about an hour. <br><br>With each level the Hyperspace Drive is developed, the speed of battleships, battlecruisers, destroyers, and deathstars are increased by 30%.";
$lang['info'][120]['name']        			= "광선 기술";
$lang['info'][120]['description'] 			= "In physics, a laser is a device that emits light through a specific mechanism for which the term laser is an acronym: Light Amplification by Stimulated Emission of Radiation. Lasers have many uses to the empire, from upgrading computer communications systems to the creation of newer weapons and space ships.";
$lang['info'][121]['name']        			= "이온 기술";
$lang['info'][121]['description'] 			= "Simply put, an ion is an atom or a group of atoms that has acquired a net electric charge by gaining or losing one or more electrons. Utilized in advanced weapons systems, a consentrated beam of Ions can cause considerable damage to objects that it strikes.";
$lang['info'][122]['name']        			= "플라즈마 기술";
$lang['info'][122]['description'] 			= "In the universe, there exists four states of matter: solid, liquids, gas, and plasma. Being an advanced version of Ion technology, Plasma Technology expands on the destructive effect that Ion Technology delivered, and opens the door to create advanced weapons systems and ships. Plasma matter is created by superheating gas and compressing it with extreme high pressures to create a sphere of superheated plasma matter. The resulting plasma sphere causes considerable damage to the target in which the sphere is launched to.";
$lang['info'][123]['name']        			= "은하 간 통신망";
$lang['info'][123]['description'] 			= "This is your deep space network to communicate researches to your colonies. With the IRN, faster research times can be achieved by linking the highest level research labs equal to the level of the IRN developed. <br><br>In order to function, each colony must be able to conduct the research independently.";
$lang['info'][124]['name']        			= "탐사 기술";
$lang['info'][124]['description'] 			= "The Expedition Technology includes several scan researches and allows you to equip different spaceships with research modules to explore uncharted regions of the universe. Those include a database and a fully functional mobile laboratory. <br><br>To assure the security of the expedition fleet during dangerous research situations, the research modules have their own energy supplies and energy field generators which creates a powerful force field around the research module during emergency situations.";
$lang['info'][199]['name']        			= "중력자 기술";
$lang['info'][199]['description'] 			= "The graviton is an elementary particle that mediates the force of gravity in the framework of quantum field theory. The graviton must be massless (because the gravitational force has unlimited range) and must have a spin of 2 (because gravity is a second-rank tensor field). Graviton Technology is only used for one thing, for the construction of the fearsome DeathStar. <br><br>Out of all of the technologies to research, this one carries the most risk of detection during the phase of preparation.";

// -------------------------- SHIPS ----------------------------------------------------------------------------------------------------//
$lang['info'][202]['name']        			= "소형 화물선";
$lang['info'][202]['description'] 			= "The first ship built by any emperor, the small cargo is an agile resource moving ship that has a cargo capacity of 5,000 resource units. This multi-use ship not only has the ability to quickly transport resources between your colonies, but also accompanies larger fleets on raiding missions on enemy targets. [Ship refitted with Impulse Drives once reached level 5]";
$lang['info'][203]['name']        			= "대형 화물선";
$lang['info'][203]['description'] 			= "As time evolved, the raids on colonies resulted in larger and larger amounts of resources being captured. As a result, Small Cargos were being sent out in mass numbers to compensate for the larger captures. It was quickly learned that a new class of ship was needed to maximize resources captured in raids, yet also be cost effective. After much development, the Large Cargo was born.<br><br>To maximize the resources that can be stored in the holds, this ship has little in the way of weapons or armor. Thanks to the highly developed combustion engine installed, it serves as the most economical resource supplier between planets, and most effective in raids on hostile worlds.";
$lang['info'][204]['name']        			= "경량 전투기";
$lang['info'][204]['description'] 			= "This is the first fighting ship all emperors will build. The light fighter is an agile ship, but vulnerable by themselves. In mass numbers, they can become a great threat to any empire. They are the first to accompany small and large cargo to hostile planets with minor defenses.";
$lang['info'][205]['name']        			= "중량 전투기";
$lang['info'][205]['description'] 			= "In developing the heavy fighter, researchers reached a point at which conventional drives no longer provided sufficient performance. In order to move the ship optimally, the impulse drive was used for the first time. This increased the costs, but also opened new possibilities. By using this drive, there was more energy left for weapons and shields; in addition, high-quality materials were used for this new family of fighters. With these changes, the heavy fighter represents a new era in ship technology and is the basis for cruiser technology.<br><br>Slightly larger than the light fighter, the heavy fighter has thicker hulls, providing more protection, and stronger weaponry.";
$lang['info'][206]['name']        			= "순양함";
$lang['info'][206]['description'] 			= "With the development of the heavy laser and the ion cannon, light and heavy fighters encountered an alarmingly high number of defeats that increased with each raid. Despite many modifications, weapons strength and armour changes, it could not be increased fast enough to effectively counter these new defensive measures. Therefore, it was decided to build a new class of ship that combined more armor and more firepower. As a result of years of research and development, the Cruiser was born. <br><br>Cruisers are armored almost three times of that of the heavy fighters, and possess more than twice the firepower of any combat ship in existence. They also possess speeds that far surpassed any spacecraft ever made. For almost a century, cruisers dominated the universe. However, with the development of Gauss cannons and plasma turrets, their predominance ended. They are still used today against fighter groups, but not as predominantly as before.";
$lang['info'][207]['name']        			= "전투선";
$lang['info'][207]['description'] 			= "Once it became apparent that the cruiser was losing ground to the increasing number of defense structures it was facing, and with the loss of ships on missions at unacceptable levels, it was decided to build a ship that could face those same type of defense structures with as little loss as possible. After extensive development, the Battleship was born. Built to withstand the largest of battles, the Battleship features large cargo spaces, heavy cannons, and high hyperdrive speed. Once developed, it eventually turned out to be the backbone of every raiding Emperors fleet.";
$lang['info'][208]['name']        			= "식민지선";
$lang['info'][208]['description'] 			= '20세기에, 사람들은 다른 행성에 가기를 원했습니다. 첫번째로는 달에 착륙했으며, 그 후에는 우주 정거장을 건설하였습니다. 그 다음으로 화성을 식민지로 만들었고, 이 것을 기초로 사람들은 다른 세계에 정착할 수 있다는 것을 확신했습니다. 전 세계의 과학자와 기술자들은 이제까지 그 어떠한 업적과도 비교가 불가능한 위대한 일을 위해 뜻을 함께 했고, 그 결과로 식민지선이 만들어 졌습니다.<br><br>이 함선을 사용하는 방법은 비어있는 목적 좌표에 함선을 보내 그 곳에 있는 행성을 자신을 위한 새로운 세계로 만드는 것 입니다.<br><font color="#ff0000">[최대 9행성을 식민지화 할 수 있습니다.]</font>';
$lang['info'][209]['name']        			= "잔해 수거선";
$lang['info'][209]['description'] 			= "As space battles became larger and more fierce, the resultant debris fields became too large to gather safely by conventional means. Normal transporters could not get close enough without receiving substantial damage. A solution was developed to this problem. The Recycler. <br><br>Thanks to the new shields and specially built equipment to gather wreckage, gathering debris no longer presented a danger. Each Recycler can gather 20,000 units of debris.";
$lang['info'][210]['name']       			= "정탐기";
$lang['info'][210]['description'] 			= "Espionage probes are small, agile drones that provide data on fleets and planets. Fitted with specially designed engines, it allows them to cover vast distances in only a few minutes. Once in orbit around the target planet, they quickly collect data and transmit the report back via your Deep Space Network for evaluation. But there is a risk to the intelligent gathering aspect. During the time the report is transmitted back to your network, the signal can be detected by the target and the probes can be destroyed.";
$lang['info'][211]['name']        			= "폭격기";
$lang['info'][211]['description'] 			= "Over the centuries, as defenses were starting to get larger and more sophisticated, fleets were starting to be destroyed at an alarming rate. It was decided that a new ship was needed to break defenses to ensure maximum results. After years of research and development, the Bomber was created.<br><br>Using laser-guided targeting equipment and Plasma Bombs, the Bomber seeks out and destroys any defense mechanism it can find. As soon as the hyperspace drive is developed to Level 8, the Bomber is retrofitted with the hyperspace engine and can fly at higher speeds.";
$lang['info'][212]['name']        			= "태양광 위성";
$lang['info'][212]['description'] 			= "It quickly became apparent that more energy was needed to power larger mines then could be produced by conventional ground based solar planets and fusion reactors. Scientists worked on the problem and discovered a method of transmitting electrical energy to the colony using specially designed satellites in geosynchronous orbit.<br><br> Solar Satellites gather solar energy and transmit it to a ground station using advanced laser technology. The efficiency of a solar satellite depends on the strength of the solar radiation it receives. In principle, energy production in orbits closer to the sun is greater than for planets in orbits distant from the sun. Since the satellites primary goal is the transmission of energy, they lack shielding and weapons capability, and because of this they are usually destroyed in large numbers in a major battle. However they do possess a small self-defense mechanism to defend itself in an espionage mission from an enemy empire if the mission is detected.";
$lang['info'][213]['name']        			= "파괴선";
$lang['info'][213]['description'] 			= "The Destroyer is the result of years of work and development. With the development of Deathstars, it was decided that a class of ship was needed to defend against such a massive weapon.Thanks to its improved homing sensors, multi-phalanx Ion cannons, Gauss Cannons and Plasma Turrets, the Destroyer turned out to be one of the most fearsome ships created.<br><br>Because the destroyer is very large, its maneuverability is severely limited, which makes it more of a battle station than a fighting ship. The lack of maneuverability is made up for by its sheer firepower, but it also costs significant amounts of deuterium to build and operate.";
$lang['info'][214]['name']        			= "죽음의 별";
$lang['info'][214]['description'] 			= "The Deathstar is the ultimate ship ever created. This moon sized ship is the only ship that can be seen with the naked eye on the ground. By the time you spot it, unfortunately, it is too late to do anything.<br><br> Armed with a gigantic graviton cannon, the most advanced weapons system ever created in the Universe, this massive ship has not only the capability of destroying entire fleets and defenses, but also has the capability of destroying entire moons. Only the most advanced empires have the capability to build a ship of this mammoth size.";
$lang['info'][215]['name']        			= "전투순양함";
$lang['info'][215]['description'] 			= "This ship is one of the most advanced fighting ships ever to be developed, and is particularly deadly when it comes to destroying attacking fleets. With its improved laser cannons on board and advanced Hyperspace engine, the Battlecruiser is a serious force to be dealt with in any attack.<br><br> Due to the ships design and its large weapons system, the cargo holds had to be cut, but this is compensated for by the lowered fuel consumption.";

// -------------------------- DEFENSES ----------------------------------------------------------------------------------------------------//
$lang['info'][401]['name']        			= "지대공 로켓 발사기";
$lang['info'][401]['description'] 			= "Your first basic line of defense. These are simple ground based launch facilities that fire conventional warhead tipped missiles at attacking enemy targets. As they are cheap to construct and no research is required, they are well suited for defending raids, but lose effectiveness defending from larger scale attacks. Once you begin construction on more advanced defense weapons systems, Rocket Launchers become simple fodder to allow your more damaging weapons to inflict greater damage for a longer period of time.<br><br>After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][402]['name']        			= "경량 광선포";
$lang['info'][402]['description'] 			= "As technology developed and more sophisticated ships were created, it was determined that a stronger line of defense was needed to counter the attacks. As Laser Technology advanced, a new weapon was designed to provide the next level of defense. Light Lasers are simple ground based weapons that utilize special targeting systems to track the enemy and fire a high intensity laser designed to cut through the hull of the target. In order to be kept cost effective, they were fitted with an improved shielding system, however the structural integrity is the same as that of the Rocket Launcher.<br><br> After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][403]['name']        			= "중량 광선포";
$lang['info'][403]['description'] 			= "The Heavy Laser is a practical, improved version of the Light Laser. Being more balanced than the Light Laser with improved alloy composition, it utilizes stronger, more densely packed beams, and even better onboard targeting systems. <br><br> After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][404]['name']        			= "가우스 포";
$lang['info'][404]['description'] 			= 'Far from being a science-fiction "weapon of tomorrow," the concept of a weapon using an electromagnetic impulse for propulsion originated as far back as the mid-to-late 1930s. Basically, the Gauss Cannon consists of a system of powerful electromagnets which fires a projectile by accelerating between a number of metal rails. Gauss Cannons fire high-density metal projectiles at extremely high velocity. <br><br>This weapon is so powerful when fired that it creates a sonic boom which is heard for miles, and the crew near the weapon must take special precautions due to the massive concussion effects generated.';
$lang['info'][405]['name']        			= "이온 포";
$lang['info'][405]['description'] 			= "An ion cannon is a weapon that fires beams of ions (positively or negatively charged particles). The Ion Cannon is actually a type of Particle Cannon; only the particles used are ionized. Due to their electrical charges, they also have the potential to disable electronic devices, and anything else that has an electrical or similar power source, using a phenomena known as the the Electromagetic Pulse (EMP effect). Due to the cannons highly improved shielding system, this cannon provides improved protection for your larger, more destructive defense weapons.<br><br> After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][406]['name']        			= "플라즈마 포탑";
$lang['info'][406]['description'] 			= "One of the most advanced defense weapons systems ever developed, the Plasma Turret uses a large nuclear reactor fuel cell to power an electromagnetic accelerator that fires a pulse, or toroid, of plasma. During operation, the Plasma turret first locks on a target and begins the process of firing. A plasma sphere is created in the turrets core by super heating and compressing gases, stripping them of their ions. Once the gas is superheated, compressed, and a plasma sphere is created, it is then loaded into the electromagnetic accelerator which is energized. Once fully energized, the accelerator is activated, which results in the plasma sphere being launched at an extremely high rate of speed to the intended target. From the targets perspective, the approaching bluish ball of plasma is impressive, but once it strikes, it causes instant destruction.<br><br> Defensive facilities deactivate as soon as they are too heavily damaged. After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][407]['name']        			= "소형 방어막 생성기";
$lang['info'][407]['description'] 			= "Colonizing new worlds brought about a new danger, space debris. A large asteroid could easily wipe out the world and all inhabitants. Advancements in shielding technology provided scientists with a way to develop a shield to protect an entire planet not only from space debris but, as it was learned, from an enemy attack. By creating a large electromagnetic field around the planet, space debris that would normally have destroyed the planet was deflected, and attacks from enemy Empires were thwarted. The first generators were large and the shield provided moderate protection, but it was later discovered that small shields did not afford the protection from larger scale attacks. The small shield dome was the prelude to a stronger, more advanced planetary shielding system to come.<br><br> After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][408]['name']        			= "대형 방어막 생성기";
$lang['info'][408]['description'] 			= "The Large Shield Dome is the next step in the advancement of planetary shields, it is the result of years of work improving the Small Shield Dome. Built to withstand a larger barrage of enemy fire by providing a higher energized electromagnetic field, large domes provide a longer period of protection before collapsing.<br><br> After a battle, there is up to a 70 % chance that failed defensive facilities can be returned to use.";
$lang['info'][502]['name']        			= "요격 미사일 [ABM]";
$lang['info'][502]['description'] 			= "Anti Ballistic Missiles (ABM) are your only line of defense when attacked by Interplanetary Missiles (IPM). When a launch of IPMs is detected, these missiles automatically arm, process a launch code in their flight computers, target the inbound IPM, and launch to intercept. During the flight, the target IPM is constantly tracked and course corrections are applied until the ABM reaches the target and destroys the attacking IPM. Each ABM destroys one incoming IPM. <br><br>Each level of your missile silo developed can store 10 ABMs, 5 IPMs, or a combination of both missile types.";
$lang['info'][503]['name']        			= "행성간 미사일 [IPM]";
$lang['info'][503]['description'] 			= "Interplanetary Missiles (IPM) are your offensive weapon to destroy the defenses of your target. Using state of the art tracking technology, each missile targets a certain number of defenses for destruction. Tipped with an anti-matter bomb, they deliver a destructive force so severe that destroyed shields and defenses cannot be repaired. The only way to counter these missiles is with APMs.<br><br> Each level of your missile silo developed can store 10 ABMs, 5 IPMs, or a combination of both missile types.";

$lang['info'][601]['name']        			= "Geologist";
$lang['info'][601]['description'] 			= 'Geologist is an expert in mining and astro crystallography. Attend their equipment in metallurgy and chemistry and is also responsible for interplanetary communications to optimize the use and refinement of raw material throughout the empire.';
$lang['info'][602]['name']        			= "Admiral";
$lang['info'][602]['description'] 			= "The Admiral is an experienced veteran and a skillful strategist. The hardest battle is able to get an idea of the situation and contact their admirals subordinates. A wise emperor could rely on their help during the fighting.";
$lang['info'][603]['name']        			= "Engineer";
$lang['info'][603]['description'] 			= "The Engineer is a specialist in energy management. In peacetime, it increases the energy of all the colonies. In case of attack, ensuring the supply of energy to the cannons, and avoid a possible overload, leading to a reduction in defense lost battle.";
$lang['info'][604]['name']        			= "Technocrat";
$lang['info'][604]['description'] 			= "The guild is composed of technocrats authentic genius, and always find that dangerous edge where it all explode into a thousand pieces before they could find an explanation and technology rational. No normal human being ever try to decode the code of a technocrat, with its presence, the researchers modeled the rule.";


//----------------------------------------------------------------------------//
//MESSAGES
$lang['mg_type'][0]    						= '정탐 보고서';
$lang['mg_type'][1]    						= '사용자로부터 온 메세지';
$lang['mg_type'][2]    						= '동맹으로부터 온 메세지';
$lang['mg_type'][3]    						= '전투 보고서';
$lang['mg_type'][4]    						= '수집 보고서';
$lang['mg_type'][5]    						= '화물 운반 보고서';
$lang['mg_type'][15]   						= '탐사 보고서';
$lang['mg_type'][99]   						= '건설 보고서';
$lang['mg_type'][100]  						= '모든 메세지';
$lang['mg_no_subject']						= '제목 없음';
$lang['mg_no_text']							= '메세지 없음';
$lang['mg_msg_sended']						= 'Message sent';
$lang['mg_delete_marked']					= '선택 안 한 메세지 삭제';
$lang['mg_delete_unmarked']					= '선택 한 메세지 삭제';
$lang['mg_delete_all']						= '모든 메세지 삭제';
$lang['mg_show_only_header_spy_reports']	= 'show only partial espionage reports ';
$lang['mg_action']							= '선택';
$lang['mg_date']							= '시간';
$lang['mg_from']							= '보낸 이';
$lang['mg_subject']							= '제목';
$lang['mg_confirm_delete']					= 'Confirm';
$lang['mg_message_title']					= '메세지';
$lang['mg_message_type']					= '메세지 종류';
$lang['mg_total']							= '총합';
$lang['mg_game_operators']					= '게임 운영자';
$lang['mg_to']								= 'To';
$lang['mg_send_message']					= 'Send message';
$lang['mg_message']							= '메세지';
$lang['mg_chars']							= 'characters';
$lang['mg_send']							= 'Send';

//----------------------------------------------------------------------------//
//ALLIANCE
$lang['al_description_message']				= 'Alliance description message';
$lang['al_web_text']						= 'Alliance web site';
$lang['al_request']							= 'Application';
$lang['al_click_to_send_request']			= 'Click here to send your application to the alliance';
$lang['al_tag_required']					= 'Alliance tag missing';
$lang['al_name_required']					= 'Alliance name missing';
$lang['al_already_exists']					= 'The alliance %s already exists.';
$lang['al_created']							= 'The alliance %s was created';
$lang['al_continue']						= 'continue';
$lang['al_alliance_closed']					= 'This alliance does not support more members';
$lang['al_request_confirmation_message']	= 'Application registered. Will receive a message when your application is approved / rejected. <br><a href="game.php?page=alliance">back</a>';
$lang['al_default_request_text']			= 'The alliance leaders have not set an example of application, or have no pretensions.';
$lang['al_write_request']					= 'Write application to the alliance %s';
$lang['al_request_deleted']					= 'Your request to the alliance %s has been deleted. <br/> Now you can write a new application or create your own alliance.';
$lang['al_request_wait_message']			= 'You\'ve already sent a request to the alliance %s. <br/> Please wait until you receive a reply or delete this application.';
$lang['al_delete_request']					= 'Delete aplication';
$lang['al_founder_cant_leave_alliance']		= 'The founder can not abandon the alliance.';
$lang['al_leave_sucess']					= 'You leaver the alliance %s succesfully.';
$lang['al_do_you_really_want_to_go_out']	= 'Do you really want to leave the alliance %s?';
$lang['al_go_out_yes']						= 'Yes';
$lang['al_circular_sended']					= 'Circular message sended, Following players will receive your circular message:';
$lang['al_all_players']						= 'All players';
$lang['al_no_ranks_defined']				= 'No ranks defined';
$lang['al_request_text']					= 'Application text';
$lang['al_inside_text']						= 'Internal text';
$lang['al_outside_text']					= 'External text';
$lang['al_transfer_alliance']				= 'Transfer alliance';
$lang['al_disolve_alliance']				= 'Disolve alliance';
$lang['al_founder_rank_text']				= 'Founder';
$lang['al_new_member_rank_text']			= 'New member';
$lang['al_acept_request']					= 'Accept';
$lang['al_you_was_acceted']					= 'You was accepted in ';
$lang['al_hi_the_alliance']					= 'Hello!<br>The alliance <b>';
$lang['al_has_accepted']					= '</b> accept you aplication<br>동맹장님의 메세지: <br>';
$lang['al_decline_request']					= 'Reject';
$lang['al_you_was_declined']				= 'You were rejected in ';
$lang['al_has_declined']					= '</b> the alliance reject your aplication<br>Founder\'s Message: <br>';
$lang['al_no_requests']						= 'No requests';
$lang['al_request_from']					= '"%s\'s" request';
$lang['al_no_request_pending']				= 'There is %n application/s pending/s';
$lang['al_name']							= 'name';
$lang['al_new_name']						= 'New name';
$lang['al_tag']								= 'tag';
$lang['al_new_tag']							= 'New tag';
$lang['al_user_list']						= 'Member List';
$lang['al_manage_alliance']					= 'manage alliance';
$lang['al_send_circular_message']			= 'send circular message';
$lang['al_new_requests']					= 'new request/s';
$lang['al_save']							= 'Save';
$lang['al_dlte']							= 'Delete';
$lang['al_rank_name']						= 'Rank name';
$lang['al_ok']								= 'OK';
$lang['al_number_of_records']				= 'Total';
$lang['al_num']								= 'ID';
$lang['al_member']							= 'Name';
$lang['al_message']							= 'Message';
$lang['al_position']						= 'Rank';
$lang['al_points']							= 'Points';
$lang['al_coords']							= 'Coords';
$lang['al_member_since']					= 'Joined';
$lang['al_estate']							= 'Online';
$lang['al_back']							= 'Back';
$lang['al_actions']							= 'Actions';
$lang['al_change_title']					= 'Change';
$lang['al_the_alliance']					= 'of the alliance';
$lang['al_change_submit']					= 'Change';
$lang['al_reply_to_request']				= 'Reply to request';
$lang['al_reason']							= 'Reason';
$lang['al_characters']						= 'characters';
$lang['al_request_list']					= 'List of requests';
$lang['al_candidate']						= 'Candidate';
$lang['al_request_date']					= 'Date of application';
$lang['al_transfer_alliance']				= 'Resign/take over this alliance?';
$lang['al_transfer_to']						= 'Transfer to';
$lang['al_transfer_submit']					= 'Transfer';
$lang['al_ally_information']				= 'Alliance information';
$lang['al_ally_info_tag']					= 'Tag';
$lang['al_ally_info_name']					= 'Name';
$lang['al_ally_info_members']				= 'Members';
$lang['al_your_request_title']				= 'Your aplication';
$lang['al_applyform_send']					= 'Send';
$lang['al_applyform_reload']				= 'Reload';
$lang['al_circular_send_ciruclar']			= 'Send circular message';
$lang['al_receiver']						= 'Recipient';
$lang['al_circular_send_submit']			= 'Send';
$lang['al_circular_reset']					= 'Clean';
$lang['al_alliance']						= 'Alliances';
$lang['al_alliance_make']					= 'Found your own alliance';
$lang['al_alliance_search']					= 'Search for alliances';
$lang['al_your_ally']						= 'Your alliance';
$lang['al_rank']							= 'Your Rank';
$lang['al_web_site']						= 'Homepage';
$lang['al_inside_section']					= 'Internal Area';
$lang['al_make_alliance']					= 'Found alliances';
$lang['al_make_ally_tag_required']			= 'Alliance Tag (3-8 characters)';
$lang['al_make_ally_name_required']			= 'Alliance name (3-30 characters)';
$lang['al_make_submit']						= 'found';
$lang['al_find_alliances']					= 'Search for alliances';
$lang['al_find_text']						= 'Search for';
$lang['al_find_submit']						= 'search';
$lang['al_the_nexts_allys_was_founded']		= 'Following alliances were found:';
$lang['al_manage_ranks']					= 'Configure rights';
$lang['al_manage_members']					= 'manage members';
$lang['al_manage_change_tag']				= 'Change the tag of the alliance';
$lang['al_manage_change_name']				= 'Change the name of the alliance';
$lang['al_texts']							= 'Text Management';
$lang['al_manage_options']					= 'Options';
$lang['al_manage_image']					= 'Alliance logo';
$lang['al_manage_requests']					= 'Applications';
$lang['al_requests_not_allowed']			= 'aren\'t possible(alliance closed)';
$lang['al_requests_allowed']				= 'are possible(alliance open)';
$lang['al_manage_founder_rank']				= 'Founder rank';
$lang['al_configura_ranks']					= 'Configure Rights';
$lang['al_create_new_rank']					= 'Create New Rank';
$lang['al_rank_name']						= 'Rank name';
$lang['al_create']							= 'Create';
$lang['al_legend']							= 'Rights Description';
$lang['al_legend_disolve_alliance']			= 'Disband alliance';
$lang['al_legend_kick_users']				= 'Kick user';
$lang['al_legend_see_requests']				= 'Show applications';
$lang['al_legend_see_users_list']			= 'Show member list';
$lang['al_legend_check_requests']			= 'Process applications';
$lang['al_legend_admin_alliance']			= 'Manage Alliance';
$lang['al_legend_see_connected_users']		= 'Show online status in member list';
$lang['al_legend_create_circular']			= 'Write circular message';
$lang['al_legend_right_hand']				= '"Right Hand" (necessary to transfer founder rank)';

$lang['al_requests']						= 'Requests';
$lang['al_circular_message']				= 'Circular message';
$lang['al_leave_alliance']					= 'Leave this alliance';

//----------------------------------------------------------------------------//
//BUDDY
$lang['bu_request_exists']					= 'There is already an application to that player!';
$lang['bu_cannot_request_yourself']			= '자신에게 친구 요청을 보낼 수 없습니다.';
$lang['bu_request_message']					= '요청 메세지';
$lang['bu_player']							= '이름';
$lang['bu_request_text']					= '응용 프로그램 문자';
$lang['bu_characters']						= 'characters';
$lang['bu_back']							= '뒤로가기';
$lang['bu_send']							= '보내다';
$lang['bu_cancel_request']					= '요청 취소';
$lang['bu_accept']							= '승낙';
$lang['bu_decline']							= '거절';
$lang['bu_connected']						= '연결됨';
$lang['bu_fifteen_minutes']					= '15 분';
$lang['bu_disconnected']					= '연결 끊김';
$lang['bu_delete']							= '삭제';
$lang['bu_buddy_list']						= '친구 목록';
$lang['bu_requests']						= '응용 프로그램';
$lang['bu_alliance']						= '동맹';
$lang['bu_coords']							= '좌표';
$lang['bu_text']							= '문자';
$lang['bu_action']							= '행동';
$lang['bu_my_requests']						= '내가 보낸 요청';
$lang['bu_partners']						= '동료';
$lang['bu_estate']							= '상태';
$lang['bu_deleted_title']					= '친구를 잃었음..';
$lang['bu_deleted_text']					= 'Player %u has deleted you from their list of friends.';
$lang['bu_accepted_title']					= '요청 승낙';
$lang['bu_accepted_text']					= 'Player %u has accepted your friend request.';
$lang['bu_rejected_title']					= '요청 거절';
$lang['bu_rejected_text']					= 'Player %u has rejected your friend request.';
$lang['bu_to_accept_title']					= '새로운 친구 요청';
$lang['bu_to_accept_text']					= 'Player %u has sent you a friend request.';

//----------------------------------------------------------------------------//
//NOTES
$lang['nt_important']						= '중요함';
$lang['nt_normal']							= '보통';
$lang['nt_unimportant']						= '낙서';
$lang['nt_create_note']						= 'Create note';
$lang['nt_edit_note']						= '메모 작성';
$lang['nt_you_dont_have_notes']				= '메모가 없습니다.';
$lang['nt_notes']							= '메모장';
$lang['nt_create_new_note']					= '새로운 메모 작성';
$lang['nt_date_note']						= '날짜';
$lang['nt_subject_note']					= '제목';
$lang['nt_size_note']						= '크기';
$lang['nt_dlte_note']						= '삭제';
$lang['nt_priority']						= '중요도';
$lang['nt_note']							= '주의';
$lang['nt_characters']						= 'characters';
$lang['nt_back']							= '뒤로가기';
$lang['nt_reset']							= '초기화';
$lang['nt_save']							= '저장';

//----------------------------------------------------------------------------//
//STATISTICS
$lang['st_player']							= '사용자';
$lang['st_alliance']						= '동맹';
$lang['st_points']							= '점수';
$lang['st_fleets']							= '함선수';
$lang['st_researh']							= '연구량';
$lang['st_buildings']						= '건물';
$lang['st_defenses']						= '방어도';
$lang['st_position']						= '순위';
$lang['st_members']							= '인원수';
$lang['st_per_member']						= '평균 점수';
$lang['st_statistics']						= '통계표';
$lang['st_updated']							= '업데이트';
$lang['st_show']							= '각';
$lang['st_per']								= '의';
$lang['st_in_the_positions']				= '순위';

//----------------------------------------------------------------------------//
//SEARCH
$lang['sh_tag']								= '태그';
$lang['sh_name']							= '이름';
$lang['sh_members']							= '인원 수';
$lang['sh_points']							= '점수';
$lang['sh_searcg_in_the_universe']			= '검색';
$lang['sh_player_name']						= '사용자명';
$lang['sh_planet_name']						= '행성명';
$lang['sh_alliance_tag']					= '동맹 태그';
$lang['sh_alliance_name']					= '동맹명';
$lang['sh_search']							= '검색';
$lang['sh_buddy_request']					= '친구 요청';
$lang['sh_alliance']						= '동맹';
$lang['sh_planet']							= '행성';
$lang['sh_coords']							= '좌표';
$lang['sh_position']						= '순위';

//----------------------------------------------------------------------------//
//OPTIONS
$lang['op_cant_activate_vacation_mode']		= 'If you\'re building or moving fleets will not be able to enter on vacation mode.';
$lang['op_password_changed']				= 'Password has been changed<br /><a href="index.php" target="_top">Back</a>';
$lang['op_username_changed']				= 'Username changed<br /><a href="index.php" target="_top">Back</a>';
$lang['op_options_changed']					= 'Changes saved.<br /><a href="game.php?page=options">Back</a>';
$lang['op_vacation_mode_active_message']	= 'The vacation mode is turned on. Have to be on vacation at least until: ';
$lang['op_end_vacation_mode']				= 'Finish vacation mode';
$lang['op_save_changes']					= 'save changes';
$lang['op_admin_title_options']				= 'Options available to management';
$lang['op_admin_planets_protection']		= 'Protection of planets';
$lang['op_user_data']						= '정보';

$lang['op_username']						= '아이디';
$lang['op_old_pass']						= '현 비밀번호';
$lang['op_new_pass']						= '새로운 비밀번호 (최소 8자 이상)';
$lang['op_repeat_new_pass']					= '새로운 비밀번호 (재입력)';
$lang['op_email_adress']					= '이메일 주소';
$lang['op_permanent_email_adress']			= '고정 주소';
$lang['op_general_settings']				= '일반 옵션';
$lang['op_sort_planets_by']					= '행성 정렬 방법:';
$lang['op_sort_kind']						= 'Assortment sequence:';
$lang['op_skin_example']					= '스킨';
$lang['op_show_skin']						= '스킨 표시';
$lang['op_deactivate_ipcheck']				= 'IP 확인 사용 안 함';
$lang['op_galaxy_settings']					= '은하 옵션';
$lang['op_spy_probes_number']				= '정탐 시 보낼 정탐기 수량';
$lang['op_toolt_data']						= '안내 툴';
$lang['op_seconds']							= '초';
$lang['op_max_fleets_messages']				= '최대 함대 메세지';
$lang['op_show_ally_logo']					= '동맹 로고 표시';
$lang['op_shortcut']						= '지름길';
$lang['op_show']							= '행동';
$lang['op_spy']								= '정탐';
$lang['op_write_message']					= '메세지 보내기';
$lang['op_add_to_buddy_list']				= '친구목록에 추가';
$lang['op_missile_attack']					= '미사일 공격';
$lang['op_send_report']						= '보내서 보내기';
$lang['op_vacation_delete_mode']			= '휴가 모드 / 계정 삭제';
$lang['op_activate_vacation_mode']			= '휴가 모드 사용';
$lang['op_dlte_account']					= '계정 삭제';
$lang['op_email_adress_descrip']			= 'You can change this email address at any time. This will be entered as a permanent address after 7 days without changes.';
$lang['op_deactivate_ipcheck_descrip']		= 'IP check means that a security logout occurs automatically when the IP changes or two people are logged into an account from different IPs. Disabling the IP check may represent a security risk!';
$lang['op_spy_probes_number_descrip']		= 'Number of espionage probes that can be sent directly from each scan in the Galaxy menu.';
$lang['op_activate_vacation_mode_descrip']	= 'Vacation mode will protect you during long absences. It can only be activated if nothing is being built (fleet, building, or defense), nothing is being researched, and none of your fleets are underway. Once it is activated, you are protected from new attacks. Attacks that have already started will be carried out. During vacation mode, production is set to zero and must be manually returned to 100 % after vacation mode ends. Vacation mode lasts a minimum of two days and can only be deactivated after this time.';
$lang['op_dlte_account_descrip']			= 'If you mark this box, your account will be deleted automatically after 7 days.';
$lang['op_sort_colonization']				= '식민지화';
$lang['op_sort_coords']						= '좌표';
$lang['op_sort_alpha']						= '알파벳';
$lang['op_sort_asc']						= 'Ascendant';
$lang['op_sort_desc']						= 'Decreasing';

//----------------------------------------------------------------------------//
//BANNED
$lang['bn_no_players_banned']				= 'No banned players';

$lang['bn_exists']							= 'Exists ';
$lang['bn_players_banned']					= ' player/s banned/s';
$lang['bn_players_banned_list']				= 'List of banned players';
$lang['bn_player']							= 'Player';
$lang['bn_reason']							= 'Reason';
$lang['bn_from']							= 'From';
$lang['bn_until']							= 'Until';
$lang['bn_by']								= 'By';

//----------------------------------------------------------------------------//
//SYSTEM
$lang['sys_attacker_lostunits'] 			= "The attacker has lost a total of";
$lang['sys_defender_lostunits'] 			= "The defender has lost a total of";
$lang['sys_units']							= "units";
$lang['debree_field_1'] 					= "A debris field";
$lang['debree_field_2']						= "floating in the orbit of the planet.";
$lang['sys_moonproba'] 						= "The probability that a moon emerge from the rubble is:";
$lang['sys_moonbuilt'] 						= "The huge amount of metal and glass are functioning and form a lunar satellite in orbit the planet %s [%d:%d:%d] !";
$lang['sys_attack_title']    				= "Fleets clash in ";
$lang['sys_attack_round']					= "Round";
$lang['sys_attack_attacker_pos'] 			= "Aggressor";
$lang['sys_attack_techologies'] 			= "Weapons: %d %% Shield: %d %% Armor: %d %% ";
$lang['sys_attack_defender_pos'] 			= "Defender";
$lang['sys_ship_type'] 						= "Type";
$lang['sys_ship_count'] 					= "Amount";
$lang['sys_ship_weapon'] 					= "Weapons";
$lang['sys_ship_shield'] 					= "Shield";
$lang['sys_ship_armour'] 					= "Armor";
$lang['sys_destroyed'] 						= "Destroyed";
$lang['fleet_attack_1'] 					= "The attacking fleet fires a total force of";
$lang['fleet_attack_2']						= "on the defender. The defender's shields absorb";
$lang['fleet_defs_1'] 						= "The defending fleet fires a total force of";
$lang['fleet_defs_2']						= "on the attacker. The attacker's shields absorb";
$lang['damage']								= "points of damage.";
$lang['sys_attacker_won'] 					= "The attacker has won the battle";
$lang['sys_defender_won'] 					= "The defender has won the battle";
$lang['sys_both_won'] 						= "The battle ended in a draw";
$lang['sys_stealed_ressources'] 			= "obtaining";
$lang['sys_and']							= "and";
$lang['sys_mess_tower'] 					= "Control Tower";
$lang['sys_mess_attack_report'] 			= "Battle Report";
$lang['sys_spy_maretials'] 					= "Resources";
$lang['sys_spy_fleet'] 						= "Fleet";
$lang['sys_spy_defenses'] 					= "Fenders";
$lang['sys_mess_qg'] 						= "Headquarters";
$lang['sys_mess_spy_report_moon']			= "(Moon)";
$lang['sys_mess_spy_report'] 				= "Report espionage";
$lang['sys_mess_spy_lostproba'] 			= "Probability of detection of the fleet of spy : %d %% ";
$lang['sys_mess_spy_control'] 				= "Space Control";
$lang['sys_mess_spy_activity'] 				= "Espionage activity";
$lang['sys_mess_spy_ennemyfleet'] 			= "An enemy fleet on the planet";
$lang['sys_mess_spy_seen_at'] 				= "was seen near your planet";
$lang['sys_mess_spy_destroyed'] 			= "Your fleet has been destroyed espionage";
$lang['sys_stay_mess_stay'] 				= "Parking Fleet";
$lang['sys_stay_mess_start'] 				= "your fleet arrives on the planet";
$lang['sys_stay_mess_end'] 					= " and offers the following resources : ";
$lang['sys_adress_planet'] 					= "[%s:%s:%s]";
$lang['sys_stay_mess_goods'] 				= "%s : %s, %s : %s, %s : %s";
$lang['sys_colo_mess_from'] 				= "Colonization";
$lang['sys_colo_mess_report'] 				= "Report of settlement";
$lang['sys_colo_defaultname'] 				= "Colony";
$lang['sys_colo_arrival'] 					= "The settlers arrived at cordenadas ";
$lang['sys_colo_maxcolo'] 					= ", but, unfortunately, can not colonize, can have no more ";
$lang['sys_colo_allisok'] 					= ", 	the settlers are beginning to build a new colony.";
$lang['sys_colo_badpos']  					= ", 	the settlers have found an environment conducive to the expansion of its empire. They decided to reverse totally disgusted ...";
$lang['sys_colo_notfree'] 					= ", 	settlers would not have found a planet with these details. They are forced to turn back completely demoralized ...";
$lang['sys_colo_planet']  					= " planet ";
$lang['sys_expe_report'] 					= "Report of expedition";
$lang['sys_recy_report'] 					= "Recycling Report";
$lang['sys_expe_blackholl_1'] 				= "The fleet was sucked into a black hole is partially destroyed.";
$lang['sys_expe_blackholl_2'] 				= "The fleet was sucked into a black hole, and was completely destroyed!";
$lang['sys_expe_nothing_1'] 				= "Your explorers took great photos. But resources have not found";
$lang['sys_expe_nothing_2'] 				= "Your scouts have spent the time in the selected area. But they have not found anything.";
$lang['sys_expe_found_goods'] 				= "The fleet has discovered an unmanned spacecraft! <br> His scouts have recovered %s de %s, %s de %s, %s de %s y %s de %s.";
$lang['sys_expe_found_ships'] 				= "Your scouts have found an abandoned squad, dominated it and come back. <br> Squadron:";
$lang['sys_expe_back_home'] 				= "Your expedition returned to the hangar.";
$lang['sys_mess_transport'] 				= "Transport Fleet";
$lang['sys_tran_mess_owner'] 				= "One of your fleet reaches %s %s and deliver their goods: %s de %s, %s de %s y %s of %s.";
$lang['sys_tran_mess_user']  				= "Found a fleet de% s% s% s coming in his libro% s% s% s units, units de% s% s% s% s units.";
$lang['sys_mess_fleetback'] 				= "Return of the fleet";
$lang['sys_tran_mess_back'] 				= "A fleet back to planet % s% s. The fleet does not give resources.";
$lang['sys_recy_gotten'] 					= "Your fleet arrived at the coordinates indicated and gatherers %s units %s and %s units of %s.";
$lang['sys_notenough_money'] 				= "You do not have enough resources to build a %s. You %s of %s, %s of %s and %s of %s and the cost of construction was %s of %s, %s of %s and %s of %s";
$lang['sys_nomore_level'] 					= "You try to destroy a building ( %s ).";
$lang['sys_buildlist'] 						= "List of building area";
$lang['sys_buildlist_fail'] 				= "Construcion impossible";
$lang['sys_gain'] 							= "Benefits";
$lang['sys_fleet_won'] 						= "One of your fleets returning from the planet %s %s and delivery %s of %s, %s of %s and %s of %s";
$lang['sys_perte_attaquant'] 				= "Forward Party";
$lang['sys_perte_defenseur'] 				= "Part Defender";
$lang['sys_debris'] 						= "Debris";
$lang['sys_destruc_title']    				= "Probability of kill moon %s :";
$lang['sys_mess_destruc_report'] 			= "Destruction Report";
$lang['sys_destruc_lune'] 					= "The probability of destroying the moon is: %d %% ";
$lang['sys_destruc_rip'] 					= "The probability that the stars of death are destroyed is: %d %% ";
$lang['sys_destruc_stop'] 					= "The defender has failed to stop the destruction of the moon";
$lang['sys_destruc_mess1'] 					= "The shooting death stars the graviton to the orbit of the moon";
$lang['sys_destruc_mess'] 					= "A fleet of the planet %s [%d:%d:%d] goes to the moon of the planet [%d:%d:%d]";
$lang['sys_destruc_echec'] 					= ". The tremors began to shake off the surface of the moon, but something goes wrong, the graviton in the stars of death also causes tremors and death stars fly to pieces.";
$lang['sys_destruc_reussi'] 				= ", The tremors began to shake off the surface of the moon, after a while the moon does not support more and fly to pieces, mission accomplished, the fleet returns to home planet.";
$lang['sys_destruc_null'] 					= ", The stars of death did not generate the power, the mission fails and the ships returned home.";
$lang['sys_the']							= " the ";
$lang['sys_stay_mess_back']         		= "One of your fleet return from ";
$lang['sys_stay_mess_bend']         		= " and offers ";

//----------------------------------------------------------------------------//
//class.CheckSession.php
$lang['ccs_multiple_users']					= 'Cookie error! There are several users with this name! You must delete your cookies. In case of problems contact the administrator.';
$lang['ccs_other_user']						= 'Cookie error! Your cookie does not match the user! You must delete your cookies. In case of problems contact the administrator';
$lang['css_different_password']				= 'Cookie error! Session error, must connect again! You must delete your cookies. In case of problems contact the administrator.';
$lang['css_account_banned_message']			= 'YOUR ACCOUNT HAS BEEN SUSPENDED';
$lang['css_account_banned_expire']			= 'Expiration:';

//----------------------------------------------------------------------------//
//class.debug.php
$lang['cdg_mysql_not_available']			= 'mySQL is not available at the moment...';
$lang['cdg_error_message']					= 'Error, please contact the administrator. Error n�:';
$lang['cdg_fatal_error']					= 'FATAL ERROR';

//----------------------------------------------------------------------------//
//class.FlyingFleetsTable.php
$lang['cff_no_fleet_data']					= 'No fleet data';
$lang['cff_aproaching']						= 'They approach ';
$lang['cff_ships']							= ' ships';
$lang['cff_from_the_planet']				= 'from the planet ';
$lang['cff_from_the_moon']					= 'from the moon ';
$lang['cff_the_planet']						= 'the planet ';
$lang['cff_debris_field']					= 'debris field ';
$lang['cff_to_the_moon']					= 'to the moon ';
$lang['cff_the_position']					= 'position ';
$lang['cff_to_the_planet']					= ' to planet ';
$lang['cff_the_moon']						= ' the moon ';
$lang['cff_from_planet']					= 'the planet ';
$lang['cff_from_debris_field']				= 'the debris field ';
$lang['cff_from_the_moon']					= 'of the moon ';
$lang['cff_from_position']					= 'position ';
$lang['cff_missile_attack']					= 'Missile attack';
$lang['cff_from']							= ' from ';
$lang['cff_to']								= ' to ';
$lang['cff_one_of_your']					= 'One of your ';
$lang['cff_a']								= 'One ';
$lang['cff_of']								= ' of ';
$lang['cff_goes']							= ' goes ';
$lang['cff_toward']							= ' toward ';
$lang['cff_with_the_mission_of']			= '. With the mission of: ';
$lang['cff_to_explore']						= ' to explore ';
$lang['cff_comming_back']					= ' comes back from ';
$lang['cff_back']							= 'Comming back';
$lang['cff_to_destination']					= 'Heading to destination';
$lang['cff_flotte'] 						= " fleets";

//----------------------------------------------------------------------------//
// EXTRA LANGUAGE FUNCTIONS
$lang['fcm_moon']							= '위성';
$lang['fcp_colony']							= '식민지';
$lang['fgp_require']						= '필요 자원: ';
$lang['fgf_time']							= '건설 시간: ';

//----------------------------------------------------------------------------//
// CombatReport.php

$lang['cr_lost_contact']					= '함대가 공격과 함께 연락이 끊겼습니다.';
$lang['cr_first_round']						= '(함대가 첫 라운드에 파괴되었습니다)';
$lang['cr_type']							= '유형';
$lang['cr_total']							= '합';
$lang['cr_weapons']							= '무기';
$lang['cr_shields']							= '방어막';
$lang['cr_armor']							= '장갑';
$lang['cr_destroyed']						= '파괴됨!';

//----------------------------------------------------------------------------//
// FleetAjax.php
$lang['fa_not_enough_probes']				= '오류, 정탐기가 부족합니다.';
$lang['fa_galaxy_not_exist']				= '오류, 은하계가 존재하지 않습니다';
$lang['fa_system_not_exist']				= '오류, 시스템이 존재하지 않습니다';
$lang['fa_planet_not_exist']				= '오류, 행성이 존재하지 않습니다';
$lang['fa_not_enough_fuel']					= '오류, 연료가 부족합니다.';
$lang['fa_no_more_slots']					= '오류, 사용할 수 있는 함대의 수가 부족합니다.';
$lang['fa_no_recyclers']					= '오류, 재생처리기가 비활성화되어있습니다.';
$lang['fa_mission_not_available']			= '오류, 임무가 비활성화되어있습니다.';
$lang['fa_no_ships']						= '오류, 임무가 비활성화되어있습니다.';
$lang['fa_vacation_mode']					= '오류, 해당 플레이어가 휴가 중 입니다.';
$lang['fa_week_player']						= '오류, 해당 플레이어가 너무 약합니다.';
$lang['fa_strong_player']					= '오류, 해당 플레이어가 너무 강합니다.';
$lang['fa_not_spy_yourself']				= '오류, 자신을 정찰 할 수 없습니다.';
$lang['fa_not_attack_yourself']				= '오류, 자신을 공격 할 수 없습니다.';
$lang['fa_action_not_allowed']				= '오류, 승인되지 않은 행동입니다.';
$lang['fa_vacation_mode_current']			= '오류, 휴가 중 입니다.';
$lang['fa_sending']							= '보내는 중';

//----------------------------------------------------------------------------//
// MissilesAjax.php
$lang['ma_silo_level']						= '미사일 격납고의 레벨이 최소 4가 되어야 합니다.';
$lang['ma_impulse_drive_required']			= '임펄스 드라이브가 필요합니다.';
$lang['ma_not_send_other_galaxy']			= '다른 은하계에 미사일을 보낼 수 없습니다.';
$lang['ma_planet_doesnt_exists']			= '행성에 존재하지 않습니다.';
$lang['ma_cant_send']						= '보낼 수 없습니다.';
$lang['ma_missile']							= ' missiles, have only ';
$lang['ma_wrong_target']					= '잘못된 표적';
$lang['ma_no_missiles']						= '사용 가능한 행성간 미사일이 없습니다.';
$lang['ma_add_missile_number']				= '당신이 발사할 미사일의 수량을 입력하세요.';
$lang['ma_misil_launcher']					= '지대공 로켓 발사기';
$lang['ma_small_laser']						= '경량 광선포';
$lang['ma_big_laser']						= '중량 광선포';
$lang['ma_gauss_canyon']					= '가우스 포';
$lang['ma_ionic_canyon']					= '이온 포';
$lang['ma_buster_canyon']					= '플라즈마 포탑';
$lang['ma_small_protection_shield']			= '소형 방어막 생성기';
$lang['ma_big_protection_shield']			= '대형 방어막 생성기';
$lang['ma_all']								= '전부';
$lang['ma_missiles_sended']					= ' 행성간 미사일을 발사했습니다. 주목표: ';
$lang["ma_all_destroyed"]  					='모든 행성간 미사일이 요격 미사일에 의해 파괴되었습니다.';
$lang['ma_planet_without_defens']  			='행성에 방어수단이 없습니다.';
$lang['ma_some_destroyed']   				=' 요격 미사일에 의해 파괴되었습니다.';
$lang['ma_missile_attack']					="";
$lang['ma_missile_string']					='(%1%)의 미사일 공격 %2%의 %3% 행성으로 <br><br>';
?>
